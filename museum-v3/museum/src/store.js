import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    email: localStorage.getItem('email') || '',
  },
  methods: {
    auth_request(){
      this.state.status = 'loading'
    },
    auth_success(state, token, email){
      this.state.status = 'success'
      this.state.token = token
      this.state.email = email
    },
    auth_error(state){
      this.state.status = 'error'
    },
    logout(state){
      this.state.status = ''
      this.state.token = ''
      this.state.email = ''
    },
  },
  actions: {
    storageToken(token, data){
      localStorage.setItem('token', data.token)
      localStorage.setItem('email', data.email)
      this.state.status = 'success'
      this.state.token = data.token
      this.state.email = data.email
    },
    deleteTokenEmail(){
      localStorage.removeItem('token')
      localStorage.removeItem('email')
      this.state.status = ''
      this.state.token = ''
      this.state.email = ''
    }
  },
  getters : {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
  }
})
