import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import PortalVue from 'portal-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueScrollTo from 'vue-scrollto'
import VeeValidate from 'vee-validate'
import store from './store'
 
Vue.use(VeeValidate);
Vue.prototype.$axios = axios
Vue.use(VueAxios, axios)
Vue.use(Vuetify)
Vue.use(BootstrapVue)
Vue.use(PortalVue)
Vue.use(VueScrollTo)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAoSVBulHHUW8HUMQsa10WMHqkXsNjcouY',
    libraries: 'places',
  },
})

Vue.config.productionTip = false
const token = localStorage.getItem('token')
if (token) {
  Vue.prototype.$axios.defaults.headers.common.Authorization = token
}
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
