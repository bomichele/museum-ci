const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const PORT = 4000
const cors = require('cors')
const mongoose = require('mongoose')
const config = require('./DB.js')

app.set('secretKey', 'D67966767A3CF87F15788BA5B7A65E36A1C960F489E585CF74DA12BDAE8D2B64'); // jwt secret token
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// use routes
var museumRoutes = require('./routes/museumRoutes');
var paintingRoutes = require('./routes/paintingRoutes');
var userRoutes = require('./routes/userRoutes');
var voteMuseumRoutes = require('./routes/voteMuseumRoutes');
var votePaintingRoute = require('./routes/votePaintingRoutes')
var wishMuseumRoutes = require('./routes/wishMuseumRoutes')
var wishPaintingRoutes = require('./routes/wishPaintingRoutes')
var userLoginRoutes = require('./routes/loginUserRoutes')

museumRoutes(app)
paintingRoutes(app)
userRoutes(app)
voteMuseumRoutes(app)
votePaintingRoute(app)
wishMuseumRoutes(app)
wishPaintingRoutes(app)
userLoginRoutes(app)

// connectiont o the database
mongoose.connect(config.DB, { useNewUrlParser: true, dbName: 'museum' }).then(
  () => { console.log('Database is connected') },
  err => { console.log('Can not connect to the database' + err) }
)


// run app
app.listen(PORT, function () {
  console.log('Server is running on Port:', PORT)
})

