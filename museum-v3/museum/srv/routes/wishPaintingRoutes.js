module.exports = function(app) {
  let wishPaintingController = require('../controllers/wishPaintingController');
	
  app.route('/api/wishPainting')
    .get(wishPaintingController.find_wishPainting)
    .post(wishPaintingController.add_wish);
  
};