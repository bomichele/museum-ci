module.exports = function(app) {
  let paintingsController = require('../controllers/paintingsController');
	
  app.route('/api/paintings')
    .get(paintingsController.find_paintings);
    
  app.route('/api/paintings/addLike')
    .post(paintingsController.add_like);
    
  app.route('/api/paintings/searchRandom')
    .get(paintingsController.search_random);
    
};