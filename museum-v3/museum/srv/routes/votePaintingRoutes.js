module.exports = function(app) {
  let votePaintingController = require('../controllers/votePaintingController');
	
  app.route('/api/votePainting')
    .get(votePaintingController.find_votePainting)
    .post(votePaintingController.add_vote);
  
};