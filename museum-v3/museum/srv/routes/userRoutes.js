module.exports = function (app) {
  let usersController = require('../controllers/usersController');
  app.route('/api/register')
    .post(usersController.register);

  app.route('/api/login')
    .post(usersController.login); 
  
  app.route('/api/users/addLogin')
    .post(usersController.login);

  app.route('/api/user')
    .post(usersController.userInfo);

  app.route('/api/addLoginUser')
    .post(usersController.add_login)
};