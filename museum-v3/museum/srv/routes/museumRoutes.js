module.exports = function(app) {
  let museumsController = require('../controllers/museumsController');
	
  app.route('/api/museums')
    .get(museumsController.find_museums);
  
  app.route('/api/museums/addLike')
    .post(museumsController.add_like);
    
};