module.exports = function(app) {
  let userLoginController = require('../controllers/userLoginController');
	
  app.route('/api/userLogin')
    .get(userLoginController.find_userLogin)
    .post(userLoginController.add_userLogin);
  
};