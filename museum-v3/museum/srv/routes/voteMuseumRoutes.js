module.exports = function(app) {
  let voteMuseumController = require('../controllers/voteMuseumController');
	
  app.route('/api/voteMuseums')
    .get(voteMuseumController.find_voteMuseum)
    .post(voteMuseumController.add_vote);
  
};