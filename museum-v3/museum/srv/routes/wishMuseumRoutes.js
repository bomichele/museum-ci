module.exports = function(app) {
  let wishMuseumController = require('../controllers/wishMuseumController');
	
  app.route('/api/wishMuseums')
    .get(wishMuseumController.find_wishMuseum)
    .post(wishMuseumController.add_wish);
  
};