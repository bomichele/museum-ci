// Require wishMuseum model in our routes module
let WishMuseum = require('../models/wishMuseumModel')

// add new wish
exports.add_wish = function(req, res){
  let wish = new WishMuseum(req.body)
  wish.save()
    .then(() => {
      res.status(200).json({ 'wish to museum': 'wish to museum in added successfully' })
    })
    .catch(() => {
      res.status(400).send('unable to save to database')
    })
}

exports.find_wishMuseum = function(req, res){
  let filter = JSON.parse(req.query.filter)
  WishMuseum.find(filter,  function (err, wish) {
    if (err) {
      res.json(err)   
    } else {
      res.json(wish)  
    }
  })
}
