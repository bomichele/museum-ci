// Require userLogin model in our routes module
let UserLogin = require('../models/userLoginModel')

// add new userLogin
exports.add_userLogin = function(req, res){
  let userLogin = new UserLogin(req.body)
  userLogin.save()
    .then(() => {
      res.status(200).json({ 'user login': 'user login in added successfully' })
    })
    .catch(() => {
      res.status(400).send('unable to save to database')
    })
}

exports.find_userLogin = function(req, res){
  let filter = JSON.parse(req.query.filter)
  UserLogin.find(filter,  function (err, userLogin) {
    if (err) {
      res.json(err)   
    } else {
      res.json(userLogin)  
    }
  })
}
