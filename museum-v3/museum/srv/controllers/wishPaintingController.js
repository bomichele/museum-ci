// Require wishPainting model in our routes module
let WishPainting = require('../models/wishPaintingModel')

// add new wish
exports.add_wish = function(req, res){
  let wish = new WishPainting(req.body)
  wish.save()
    .then(() => {
      res.status(200).json({ 'wish to painting': 'wish to painting in added successfully' })
    })
    .catch(() => {
      res.status(400).send('unable to save to database')
    })
}

exports.find_wishPainting = function(req, res){
  let filter = JSON.parse(req.query.filter)
  WishPainting.find(filter,  function (err, wish) {
    if (err) {
      res.json(err)   
    } else {
      res.json(wish)  
    }
  })
}
