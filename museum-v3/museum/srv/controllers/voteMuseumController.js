// Require voteMuseum model in our routes module
let VoteMuseum = require('../models/voteMuseumModel')

// add new vote
exports.add_vote = function(req, res){
  let vote = new VoteMuseum(req.body)
  vote.save()
    .then(() => {
      res.status(200).json({ 'vote to museum': 'vote to museum in added successfully' })
    })
    .catch(() => {
      res.status(400).send('unable to save to database')
    })
}

exports.find_voteMuseum = function(req, res){
  let filter = JSON.parse(req.query.filter)
  VoteMuseum.find(filter,  function (err, vote) {
    if (err) {
      res.json(err)   
    } else {
      res.json(vote)  
    }
  })
}
