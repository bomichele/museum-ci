// Require painting model in our routes module
let Painting = require('../models/paintingModel')

exports.find_paintings = function(req, res){
  let filter = JSON.parse(req.query.filter)
  Painting.find(filter,  function (err, paintings) {
    if (err) {
      res.json(err)   
    } else {
      res.json(paintings)  
    }
  }).sort(JSON.parse(req.query.sort))
    .limit(JSON.parse(req.query.limit))
}

exports.add_like = function(req, res){
  Painting.findOneAndUpdate({pictureLabel: req.body.name}, {$set:{like: req.body.like}}, {new: true}, (err, painting) => {
    if (err)
      res.send(err);
    else if (painting===null){
      res.status(404).send({
        description: 'Movie not found'
      });
    }
    else {
      res.json(painting);
    }
  })
}

exports.search_random = function(req, res){
  Painting.findRandom({}, {}, {limit: 4}, function(err, paintings) {
    if (err) {
      res.json(err)   
    } else {
      res.json(paintings)  
    }
  });
}