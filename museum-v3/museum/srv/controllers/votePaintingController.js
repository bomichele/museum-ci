// Require votePainting model in our routes module
let VotePainting = require('../models/votePaintingModel')

// add new vote
exports.add_vote = function(req, res){
  let vote = new VotePainting(req.body)
  vote.save()
    .then(() => {
      res.status(200).json({ 'vote to painting': 'vote to painting in added successfully' })
    })
    .catch(() => {
      res.status(400).send('unable to save to database')
    })
}

exports.find_votePainting = function(req, res){
  let filter = JSON.parse(req.query.filter)
  VotePainting.find(filter,  function (err, vote) {
    if (err) {
      res.json(err)   
    } else {
      res.json(vote)  
    }
  })
}
