const userModel = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
  register: function (req, res, next) {
    userModel.create({ name_surname: req.body.name, username: req.body.username, email: req.body.email, password: req.body.password, exp: 0, numberLogin: req.body.numberLogin }, function (err, result) {
      if (err)
        next(err);

      else
        res.json({ status: "success", message: "User added successfully!", data: null });

    });
  },

  login (req, res, next) {
    userModel.findOne({ email: req.body.email }, function (err, userInfo) {
      if (userInfo == null) {
        res.json({ status: "error", message: "Invalid email or password!", data: null });
        next();
      } else {
        if (bcrypt.compareSync(req.body.password, userInfo.password)) {
          const token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '1h' });
          res.json({ status: "success", message: "User found!", data: { token: token } });
        } else {
          res.json({ status: "error", message: "Invalid email or password!", data: null });
        }
      }
    });
  },


  add_login(req, res){
    userModel.findOneAndUpdate({email: req.body.email}, { $inc: {numberLogin:1}}, {new: true}, (err, user) => {
      if (err)
        res.send(err);
      else if (user===null){
        res.status(404).send({
          description: 'User not found'
        });
      }
      else {
        res.json(user);
      }

    })
  },

  userInfo(req, res, next) {
    userModel.findOne({ email: req.body.email }, '-password', function (err, userInfo) {
      if (userInfo === null) {
        res.json({ status: "error", message: "User info not found", data: null });
        next(err);
      } else {
        res.json({ status: "success", message: "User info found", data: { userInfo: userInfo } });
      }        

    })
  }
}


