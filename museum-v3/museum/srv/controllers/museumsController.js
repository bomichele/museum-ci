// Require museum model in our routes module
let Museum = require('./../models/museumModel')

exports.find_museums = function(req, res){
  let filter = JSON.parse(req.query.filter)
  Museum.find(filter,function (err, museums) {
    if (err) {
      res.json(err)   
    } else {
      res.json(museums)  
    }
  }).sort(JSON.parse(req.query.sort))
    .limit(JSON.parse(req.query.limit))
}

exports.add_like = function(req, res){
  Museum.findOneAndUpdate({name: req.body.name}, {$set:{like: req.body.like}}, {new: true}, (err, museum) => {
    if (err)
      res.send(err);
    else if (museum===null){
      res.status(404).send({
        description: 'Painting not found'
      });
    }
    else {
      res.json(museum);
    }
  });
}
