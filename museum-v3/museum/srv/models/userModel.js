const bcrypt = require('bcrypt');
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema

var UserSchema = new Schema({
  name_surname: {
    type: String,
    trim: true,
    required: true,
  },
  username: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    trim: true,
    required: true,
    unique:true
  },
  password: {
    type: String,
    trim: true,
    required: true
  },
  exp: {
    type: Number,
    required: true,
  },
  numberLogin:{
    type: Number,
    required: true,

  }
});

// hash user password before saving into database
UserSchema.pre('save', function (next) {
  this.password = bcrypt.hashSync(this.password, 10);
  next();
});

UserSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', UserSchema)

