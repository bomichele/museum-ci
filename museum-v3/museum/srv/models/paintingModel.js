var mongoose = require('mongoose')
var Schema = mongoose.Schema

var PaintingSchema = new Schema({

  pictureLabel: {
    type: String,
    required: 'The name of the painting is required'
  },

  creatorLabel: {
    type: String,
    required: 'The creator of the painting is required'
  },

  image: {
    type: String,
    required: 'The name of the painting is required'
  },

  year: {
    type: String
  },

  movementLabel: {
    type: String,
    required: 'The name of the painting is required'
  },

  museumLabel: {
    type: String,
    required: 'The name of the painting is required'
  },
  like:{
    type: Number
  }
}
)
PaintingSchema.plugin(require('mongoose-simple-random'));
module.exports = mongoose.model('painting', PaintingSchema)
