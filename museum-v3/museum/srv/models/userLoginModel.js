var mongoose = require('mongoose')
var Schema = mongoose.Schema

var userLogin = new Schema({
  date: {
    type: String,
    require: 'The data is required'
  },
  email: {
    type: String,
    required: 'The email of the user is required'
  }

}
)

module.exports = mongoose.model('userLogin', userLogin)
