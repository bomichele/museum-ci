var mongoose = require('mongoose')
var Schema = mongoose.Schema

var MuseumSchema = new Schema({

  name: {
    type: String,
    required: 'The name of the museum is required'
  },

  countryLabel: {
    type: String
  },

  coordinateLocation: {
    type: String
  },

  price: {
    type: String
  },

  openDays: {
    type: String
  },

  closeOn: {
    type: String
  },

  openFrom: {
    type: String
  },

  openTo: {
    type: String
  },

  accessibilityLabel: {
    type: String
  },

  website: {
    type: String
  },

  phone: {
    type: String
  },

  image: {
    type: String
  },

  email: {
    type: String
  },

  logo: {
    type: String
  },
  like:{
    type: Number
  }
})

module.exports = mongoose.model('museum', MuseumSchema)
