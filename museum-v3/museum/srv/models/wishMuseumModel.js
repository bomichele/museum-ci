var mongoose = require('mongoose')
var Schema = mongoose.Schema

var WishMuseumSchema = new Schema({

  museumName: {
    type: String,
    required: 'The name of the painting is required'
  },
  email: {
    type: String,
    required: 'The email of the user is required'
  }

}
)

module.exports = mongoose.model('wishmuseum', WishMuseumSchema)
